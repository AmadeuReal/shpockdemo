# Shpock iOS Technical Assessment

## Detail
This repo contains the implementation of the Shpock iOS Techical Assessment, its requirement can be seen in here: [iOS_Exercise.pdf](./iOS_Interview_Task.pdf).

## Getting started

This project contains no external dependencies. 
It's comprised by 3 targets: **ShpockDemo**, **ShpockDemoTests** and **ShpockDemoUITests**.

### Main target

The application's architecture follows the assessment constraint, it's using an **MVVM** using **Combine** to bind our ViewModel with the Views, alongside the creational pattern **Factory** and the Coordinator pattern to handle the navigation funcionality.

#### App funcionalities

- Fetch for pirate ships and its image from a Network service.
- Handles caching.
- Allow to filter the list by text.
- Navigate to the Ship's detail screen.
- Allow the user to favourite a ship from the detail.
- Allow the user to filter all ships by his favourite.

### Testing targets 

- Under the **ShpockDemoTests**, the unit tests are being performed on the models, the business logic inside the view models, the construction of the view controllers on the factory and requesters, validating the behaviour when any type of response is provided.

- Under the **ShpockDemoUITests**, I've done a small user journey that warranties that main business goal is performed under every commercial release. Besides that I've also created a performance test that allow us to keep an eye on the performance of some basic action of the app.
