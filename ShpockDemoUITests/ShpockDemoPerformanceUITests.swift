//
//  ShpockDemoPerformanceUITests.swift
//  ShpockDemoUITests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

import ShpockDemo
import XCTest

class ShpockDemoPerformanceUITests: XCTestCase {

    let app = XCUIApplication()

    func testLaunchPerformance() throws {

        self.measure(metrics: [XCTApplicationLaunchMetric()]) {

            self.app.launch()

            self.performSimpleJourney(usingCellAt: 0)

            self.app.terminate()
        }
    }

    private func performSimpleJourney(usingCellAt row: Int) {

        self.app.collectionViews.cells.element(boundBy: row).tap()

        self.app.buttons.matching(identifier: A11y.PirateDetail.greetingCTA).firstMatch.tap()

        self.app.alerts.buttons.firstMatch.tap()

        self.app.buttons.firstMatch.tap()
    }
}
