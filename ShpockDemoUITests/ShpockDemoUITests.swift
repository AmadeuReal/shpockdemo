//
//  ShpockDemoUITests.swift
//  ShpockDemoUITests
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import XCTest

class ShpockDemoUITests: XCTestCase {

    let app = XCUIApplication()

    func setup() {

        super.setUp()
        self.continueAfterFailure = false
    }

    func testExample() throws {

        self.app.launch()

        self.performSimpleJourney(usingCellAt: 0)

        self.performSimpleJourney(usingCellAt: 3)

        self.performSimpleJourney(usingCellAt: 5)

        self.app.terminate()
    }

    private func performSimpleJourney(usingCellAt row: Int) {

        self.app.collectionViews.cells.element(boundBy: row).tap()

        self.app.buttons.matching(identifier: A11y.PirateDetail.greetingCTA).firstMatch.tap()

        self.app.alerts.firstMatch.buttons.firstMatch.tap()

        self.app.buttons.firstMatch.tap()
    }
}
