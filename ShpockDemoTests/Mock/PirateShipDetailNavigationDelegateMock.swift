//
//  PirateShipDetailNavigatorMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

@testable import ShpockDemo

final class PirateShipDetailNavigatorMock: PirateShipDetailNavigator {

    var onDidTapToShowAlert: ((String, String) -> Void)!

    func showAlertController(with title: String, description: String) {

        self.onDidTapToShowAlert(title, description)
    }
}
