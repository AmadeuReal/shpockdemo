//
//  StorageMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 14/11/2020.
//

import Foundation
@testable import ShpockDemo

final class StorageMock: Storage {

    private var dictionary = [String: Any]()

    func save<T: Codable>(_ value: T, in key: String) {

        self.dictionary[key] = value as Any
    }

    func value<T: Codable>(at key: String) throws -> T {

        guard let value = self.dictionary[key] as? T else {

            throw StorageError.notFound
        }
        
        return value
    }

    func deleteValue(fromKey key: String) {

        self.dictionary.removeValue(forKey: key)
    }
}
