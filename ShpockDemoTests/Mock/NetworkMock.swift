//
//  NetworkMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Combine
import Foundation
@testable import ShpockDemo

final class NetworkMock: NetworkType {

    var onRequestCallback: (() -> AnyPublisher<Data, Error>)!

    func request(with request: URLRequest, queue: DispatchQueue) -> AnyPublisher<Data, Error> {

        self.onRequestCallback()
    }
}
