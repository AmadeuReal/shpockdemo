//
//  PirateShipsRequesterMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Combine
@testable import ShpockDemo

final class PirateShipsRequesterMock: PirateShipsRequesterType {

    var onShipsCallback: (() -> AnyPublisher<[PirateShip], Error> )!

    func fetchShips() -> AnyPublisher<[PirateShip], Error>  {

        self.onShipsCallback()
    }
}
