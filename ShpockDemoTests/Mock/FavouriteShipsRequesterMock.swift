//
//  FavouriteShipsRequesterMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 14/11/2020.
//

import Combine
import Foundation
@testable import ShpockDemo

final class FavouriteShipsRequesterMock: FavouriteShipsRequesterType {

    var onIsShipFavouritedCallback: (() -> AnyPublisher<Bool, Error>)!
    var onSaveFavouriteShipCallback: (() -> AnyPublisher<Void, Error>)!
    var onRemoveFromFavouriteShipCallback: (() -> AnyPublisher<Void, Error>)!
    var onFetchFavouritedShipIdsCallback: (() -> AnyPublisher<[Int], Error>)!

    func isShipFavourited(id: Int) -> AnyPublisher<Bool, Error> {

        self.onIsShipFavouritedCallback()
    }

    func saveFavouriteShip(id: Int) -> AnyPublisher<Void, Error> {

        self.onSaveFavouriteShipCallback()
    }

    func removeFromFavouriteShip(id: Int) -> AnyPublisher<Void, Error> {

        self.onRemoveFromFavouriteShipCallback()
    }

    func fetchFavouritedShipIds() -> AnyPublisher<[Int], Error> {

        self.onFetchFavouritedShipIdsCallback()
    }
}
