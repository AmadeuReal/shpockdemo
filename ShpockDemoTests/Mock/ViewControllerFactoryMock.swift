//
//  ViewControllerFactoryMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

import UIKit
@testable import ShpockDemo

final class ViewControllerFactoryMock: ViewControllerFactory {

    var onBuildPirateShipsListViewControllerCallback: (() -> UIViewController)!
    var onBuildPirateShipDetailViewControllerCallback: ((PirateShip) -> UIViewController)!

    func buildPirateShipsListViewController(navigator: PirateShipsListingNavigator) -> UIViewController {

        self.onBuildPirateShipsListViewControllerCallback()
    }

    func buildPirateShipDetailViewController(
        with pirateShip: PirateShip,
        navigator: PirateShipDetailNavigator
    ) -> UIViewController {

        self.onBuildPirateShipDetailViewControllerCallback(pirateShip)
    }
}
