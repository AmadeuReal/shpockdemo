//
//  ImageRequesterMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Combine
import UIKit
@testable import ShpockDemo

final class ImageRequesterMock: ImageRequesterType {

    var onFetchImageCallback: (() -> AnyPublisher<UIImage, Error>)!

    func fetchImage(from url: String) -> AnyPublisher<UIImage, Error> {

        self.onFetchImageCallback()
    }
}

enum AssetsBook {

    static let mock = UIImage(
        named: "img.jpg",
        in: Bundle(for: ImageRequesterMock.self),
        with: nil
    )!
}
