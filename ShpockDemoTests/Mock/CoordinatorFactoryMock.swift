//
//  CoordinatorFactoryMock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

@testable import ShpockDemo

final class CoordinatorFactoryMock: CoordinatorFactory {

    var onBuildRootCoordinatorCallback: (() -> Coordinator)!

    func buildRootCoordinator() -> Coordinator {

        self.onBuildRootCoordinatorCallback()
    }
}
