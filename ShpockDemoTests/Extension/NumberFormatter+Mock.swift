//
//  NumberFormatter+Mock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Foundation

extension NumberFormatter {

    static let mock: NumberFormatter = {

        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .currency
        return formatter
    }()
}
