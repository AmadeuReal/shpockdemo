//
//  AnyPublisher.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 14/11/2020.
//

import Combine

extension AnyPublisher {

    static func success<T>(_ object: T) -> AnyPublisher<T, Failure> {

        Future { $0(.success(object)) }.eraseToAnyPublisher()
    }

    static func failure<E>(_ error: E) -> AnyPublisher<Output, E> {

        Future { $0(.failure(error)) }.eraseToAnyPublisher()
    }
}
