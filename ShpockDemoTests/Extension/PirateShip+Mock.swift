//
//  PirateShip+Mock.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 11/11/2020.
//

@testable import ShpockDemo

extension PirateShip {

    static func mock(
        id: Int,
        title: String? = nil,
        description: String = "Random Description",
        price: Double,
        image: String = "",
        greeting: PirateShip.Greeting? = nil
    ) -> PirateShip {

        .init(id: id, title: title, description: description, price: price, imageUrl: image, greeting: greeting)
    }
}
