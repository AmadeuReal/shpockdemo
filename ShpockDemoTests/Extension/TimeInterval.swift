//
//  TimeInterval.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Foundation

extension TimeInterval {

    static let quickTimeout: TimeInterval = 0.5
}
