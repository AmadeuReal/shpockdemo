//
//  UnitTestingAppDelegate.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import UIKit

class UnitTestingAppDelegate: UIResponder, UIApplicationDelegate {

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        return true
    }
}
