//
//  ImageRequesterTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

import Combine
import XCTest
@testable import ShpockDemo

final class ImageRequesterTests: XCTestCase {

    var network: NetworkMock!
    var cancellables = [AnyCancellable]()

    override func setUp() {

        super.setUp()

        self.network = NetworkMock()
        
        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()
    }

    func testInvalidData() {

        let expectation = XCTestExpectation(
            description: "it should return a typed error when data is invalid"
        )

        let imageRequester = ImageRequester(network: self.network, queue: .main)

        self.network.onRequestCallback = { .success(Data()) }

        imageRequester.fetchImage(from: "random").sink(receiveCompletion: { completion in

            guard case .failure(NetworkingError.badData) = completion else { return }
            expectation.fulfill()

        }, receiveValue: { _ in XCTFail("Fetch image should have failed when data is invalid") })
        .store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testDataTransformationToUImageObject() throws {

        let expectation = XCTestExpectation(
            description: "it should return a valid UIImage from data"
        )

        let imageData = try XCTUnwrap(AssetsBook.mock.pngData())
        self.network.onRequestCallback = { .success(imageData) }

        let imageRequester = ImageRequester(network: self.network, queue: .main)

        imageRequester.fetchImage(from: "random").sink(receiveCompletion: { completion in

            guard case .failure(NetworkingError.badData) = completion else { return }
            XCTFail("Fetch image should have not failed when data is invalid")

        }, receiveValue: { image in

            guard imageData ==  image.pngData() else { return }
            expectation.fulfill()

        }).store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testCachedImage() throws {

        let expectation = XCTestExpectation(
            description: "it should not perform for an image that is already cached"
        )

        let imageData = try XCTUnwrap(AssetsBook.mock.pngData())

        var numberOfRequests = 0
        self.network.onRequestCallback = {

            numberOfRequests += 1
            return .success(imageData)
        }

        let imageRequester = ImageRequester(network: self.network, queue: .main)

        imageRequester.fetchImage(from: "random").flatMap { _ in

            imageRequester.fetchImage(from: "random")

        }.sink { completion in

            guard case .failure = completion else { return }
            XCTFail("Fetch image was not supposed to fail")

        } receiveValue: { _ in

            expectation.fulfill()

        }.store(in: &self.cancellables)


        wait(for: [expectation], timeout: .quickTimeout)

        XCTAssertEqual(numberOfRequests, 1)
    }
}
