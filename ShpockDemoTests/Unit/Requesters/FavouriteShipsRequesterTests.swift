//
//  FavouriteShipsRequesterTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 14/11/2020.
//

import Combine
import XCTest
@testable import ShpockDemo

final class FavouriteShipsRequesterTests: XCTestCase {

    var cancellables = [AnyCancellable]()

    var storage: StorageMock!

    override func setUp() {

        super.setUp()

        self.storage = StorageMock()

        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()
    }

    func testEmptyValues() {

        let expectation = XCTestExpectation(description: "It should return a success when no favourite exists")

        let favouriteRequester = FavouriteShipsRequester(storage: self.storage)

        favouriteRequester.fetchFavouritedShipIds().sink { _ in

        } receiveValue: { ids in

            XCTAssertEqual(ids, [])
            expectation.fulfill()

        }.store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testAddFavouriteId() {

        let expectation = XCTestExpectation(description: "It should return a favourite when added to the list")

        let favouriteRequester = FavouriteShipsRequester(storage: self.storage)

        favouriteRequester.saveFavouriteShip(id: 1).flatMap {

            favouriteRequester.fetchFavouritedShipIds()

        }.sink { _ in

        } receiveValue: { ids in

            XCTAssertEqual(ids, [1])
            expectation.fulfill()

        }.store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testAppendFavouriteId() {

        let expectation = XCTestExpectation(description: "It should append to the list of favourites")

        let favouriteRequester = FavouriteShipsRequester(storage: self.storage)

        favouriteRequester.saveFavouriteShip(id: 1).flatMap {

            favouriteRequester.saveFavouriteShip(id: 2)

        }.flatMap {

            favouriteRequester.fetchFavouritedShipIds()

        }.sink { _ in

        } receiveValue: { ids in

            XCTAssertEqual(ids, [1, 2])
            expectation.fulfill()

        }.store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testRemovingAnItem() {

        let expectation = XCTestExpectation(description: "It should remove successfully an id from list of favourites")

        let favouriteRequester = FavouriteShipsRequester(storage: self.storage)

        favouriteRequester.saveFavouriteShip(id: 1).flatMap {

            favouriteRequester.saveFavouriteShip(id: 2)

        }.flatMap {

            favouriteRequester.removeFromFavouriteShip(id: 1)

        }.flatMap {

            favouriteRequester.fetchFavouritedShipIds()

        }.sink { _ in

        } receiveValue: { ids in

            XCTAssertEqual(ids, [2])
            expectation.fulfill()

        }.store(in: &self.cancellables)
    }

    func testValidateWhetherIdIsFavourited() {

        let expectation = XCTestExpectation(description: "It should validate whether an id is on to the list of favourites")

        let favouriteRequester = FavouriteShipsRequester(storage: self.storage)

        favouriteRequester.saveFavouriteShip(id: 1).flatMap {

            favouriteRequester.saveFavouriteShip(id: 2)

        }.flatMap {

            favouriteRequester.removeFromFavouriteShip(id: 1)

        }.flatMap {

            favouriteRequester.isShipFavourited(id: 1)

        }.sink { _ in

        } receiveValue: { isFavourited in

            XCTAssertEqual(isFavourited, false)
            expectation.fulfill()

        }.store(in: &self.cancellables)
    }
}
