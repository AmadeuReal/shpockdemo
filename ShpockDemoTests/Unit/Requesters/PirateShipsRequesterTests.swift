//
//  PirateShipsRequesterTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

import Combine
import XCTest
@testable import ShpockDemo

final class PirateShipsRequesterTests: XCTestCase {

    var cancellables = [AnyCancellable]()
    let decoder = JSONDecoder()
    var network: NetworkMock!
    lazy var bundle = Bundle(for: Self.self)

    override func setUp() {

        super.setUp()

        self.network = NetworkMock()

        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()
    }

    func testHandleResponseFromFile() throws {

        let expectation = XCTestExpectation(
            description: "it should be able to process the data into a valid [Ship] model"
        )

        let requester = PirateShipsRequester(network: self.network, decoder: self.decoder, queue: .main)

        let data = try self.getData()
        self.network.onRequestCallback = { .success(data) }

        requester.fetchShips().sink(receiveCompletion: { _ in

        }, receiveValue: { ships in

            XCTAssertEqual(ships.count, 10)
            expectation.fulfill()

        }).store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testReturnAnErrorWhenDataIsInvalid() throws {

        let expectation = XCTestExpectation(
            description: "it should be able to return a typed error when it's not possible to create the [Ship] models"
        )

        let requester = PirateShipsRequester(network: self.network, decoder: self.decoder, queue: .main)

        self.network.onRequestCallback = { .success(Data()) }

        requester.fetchShips().sink(receiveCompletion: { completion in

            guard case .failure = completion else { return }
            expectation.fulfill()

        }, receiveValue: { ships in

            XCTFail("Fetch ships should have failed but return ships: \(ships)")

        }).store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    private func getData() throws -> Data {

        let fileURL = try XCTUnwrap(self.bundle.url(forResource: "sample_page_request", withExtension: "json"))
        return try Data(contentsOf: fileURL)
    }
}
