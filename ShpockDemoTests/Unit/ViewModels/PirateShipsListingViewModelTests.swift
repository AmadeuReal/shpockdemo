//
//  PirateShipsListingViewModelTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Combine
import XCTest
@testable import ShpockDemo

final class PirateShipsListingViewModelTests: XCTestCase {

    var imageRequester: ImageRequesterMock!
    var shipsRequester: PirateShipsRequesterMock!
    var favouriteShipsRequester: FavouriteShipsRequesterMock!

    var selectionPublisher: PassthroughSubject<Int, Never>!
    var searchPublisher: PassthroughSubject<String, Never>!
    var favouritePublisher: PassthroughSubject<Void, Never>!

    var cancellables = [AnyCancellable]()

    override func setUp() {
        super.setUp()

        self.imageRequester = ImageRequesterMock()
        self.shipsRequester = PirateShipsRequesterMock()
        self.favouriteShipsRequester = FavouriteShipsRequesterMock()

        self.selectionPublisher = PassthroughSubject<Int, Never>()
        self.searchPublisher = PassthroughSubject<String, Never>()
        self.favouritePublisher = PassthroughSubject<Void, Never>()

        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()
    }

    func testHandleEmptyResponse() {

        self.shipsRequester.onShipsCallback = { .success([]) }

        let viewModel = PirateShipsListingViewModel(
            priceFormatter: .mock,
            shipsRequester: self.shipsRequester,
            imageRequester: self.imageRequester,
            favouriteShipsRequester: self.favouriteShipsRequester
        )

        let expectation = XCTestExpectation(description: "it should be able to handle an empty response")

        viewModel.bind(
            input: .init(
                selection: self.selectionPublisher.eraseToAnyPublisher(),
                search: self.searchPublisher.eraseToAnyPublisher(),
                favouriteFilter: self.favouritePublisher.eraseToAnyPublisher()
            )
        ).sink { state in

            guard case .success = state else { return }
            expectation.fulfill()

        }.store(in: &self.cancellables)

        self.searchPublisher.send("")

        wait(for: [expectation], timeout: .quickTimeout)

        XCTAssertTrue(viewModel.pirateShipsViewModel.isEmpty)
    }

    func testHandleErrorResponse() {

        self.shipsRequester.onShipsCallback = { .failure(NetworkingError.generic) }

        let viewModel = PirateShipsListingViewModel(
            priceFormatter: .mock,
            shipsRequester: self.shipsRequester,
            imageRequester: self.imageRequester,
            favouriteShipsRequester: self.favouriteShipsRequester
        )

        let expectation = XCTestExpectation(description: "it should be able to handle an empty response")

        viewModel.bind(
            input: .init(
                selection: self.selectionPublisher.eraseToAnyPublisher(),
                search: self.searchPublisher.eraseToAnyPublisher(),
                favouriteFilter: self.favouritePublisher.eraseToAnyPublisher()
            )
        ).sink { state in

            guard case .success = state else { return }
            expectation.fulfill()

        }.store(in: &self.cancellables)

        self.searchPublisher.send("")

        wait(for: [expectation], timeout: .quickTimeout)

        XCTAssertTrue(viewModel.pirateShipsViewModel.isEmpty)
    }

    func testTransformationFromModelToViewModel() {

        let expectedResponse = (0 ... 5).map { PirateShip.mock(id: $0, price: 20) }

        self.shipsRequester.onShipsCallback = { .success(expectedResponse) }
        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }

        let viewModel = PirateShipsListingViewModel(
            priceFormatter: .mock,
            shipsRequester: self.shipsRequester,
            imageRequester: self.imageRequester,
            favouriteShipsRequester: self.favouriteShipsRequester
        )

        let expectation = XCTestExpectation(description: "it should be able to build ViewModel from Ship models")

        viewModel.bind(
            input: .init(
                selection: self.selectionPublisher.eraseToAnyPublisher(),
                search: self.searchPublisher.eraseToAnyPublisher(),
                favouriteFilter: self.favouritePublisher.eraseToAnyPublisher()
            )
        ).sink { state in

            guard case .success = state else { return }
            expectation.fulfill()
            
        }.store(in: &self.cancellables)

        self.searchPublisher.send("")

        wait(for: [expectation], timeout: .quickTimeout)

        XCTAssertEqual(viewModel.pirateShipsViewModel.count, expectedResponse.count)
    }

    func testWhenSelectingAValidShip() {

        self.shipsRequester.onShipsCallback = { .success((0 ... 5).map { PirateShip.mock(id: $0, price: 20) }) }
        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }

        let viewModel = PirateShipsListingViewModel(
            priceFormatter: .mock,
            shipsRequester: self.shipsRequester,
            imageRequester: self.imageRequester,
            favouriteShipsRequester: self.favouriteShipsRequester
        )

        let expectation = XCTestExpectation(
            description: "it should be call the navigation delegate with the expect model selected"
        )

        let delegate = PirateShipsListingNavigatorMock()
        viewModel.navigator = delegate

        viewModel.bind(
            input: .init(
                selection: self.selectionPublisher.eraseToAnyPublisher(),
                search: self.searchPublisher.eraseToAnyPublisher(),
                favouriteFilter: self.favouritePublisher.eraseToAnyPublisher()
            )
        ).sink { state in

            guard case .success = state else { return }
            self.selectionPublisher.send(1)
            
        }.store(in: &self.cancellables)

        self.searchPublisher.send("")

        delegate.onDidTap = { ship in

            guard ship.id == 1 else { return }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testFilterFavourited() {

        self.favouriteShipsRequester.onFetchFavouritedShipIdsCallback = { .success([1, 2])}

        self.shipsRequester.onShipsCallback = { .success((0 ... 5).map { PirateShip.mock(id: $0, price: 20) }) }
        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }

        let viewModel = PirateShipsListingViewModel(
            priceFormatter: .mock,
            shipsRequester: self.shipsRequester,
            imageRequester: self.imageRequester,
            favouriteShipsRequester: self.favouriteShipsRequester
        )

        let expectation = XCTestExpectation(
            description: "it should filter and display only the favourited ships"
        )

        let delegate = PirateShipsListingNavigatorMock()
        viewModel.navigator = delegate

        var shouldFavourited = true
        viewModel.bind(
            input: .init(
                selection: self.selectionPublisher.eraseToAnyPublisher(),
                search: self.searchPublisher.eraseToAnyPublisher(),
                favouriteFilter: self.favouritePublisher.eraseToAnyPublisher()
            )
        ).sink { state in

            guard case .success = state else { return }

            if shouldFavourited {

                shouldFavourited.toggle()
                self.favouritePublisher.send(())
            }

            if shouldFavourited == false { expectation.fulfill() }

        }.store(in: &self.cancellables)

        self.searchPublisher.send("")

        wait(for: [expectation], timeout: .quickTimeout)

        XCTAssertEqual(viewModel.pirateShipsViewModel.count, 2)
    }

    func testFilterBySearchTerm() {

        self.shipsRequester.onShipsCallback = { .success((0 ... 5).map { PirateShip.mock(id: $0, title: "ship \($0)", price: 20) }) }
        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }

        let viewModel = PirateShipsListingViewModel(
            priceFormatter: .mock,
            shipsRequester: self.shipsRequester,
            imageRequester: self.imageRequester,
            favouriteShipsRequester: self.favouriteShipsRequester
        )

        let expectation = XCTestExpectation(
            description: "it should filter and display only the favourited ships"
        )

        let delegate = PirateShipsListingNavigatorMock()
        viewModel.navigator = delegate

        viewModel.bind(
            input: .init(
                selection: self.selectionPublisher.eraseToAnyPublisher(),
                search: self.searchPublisher.eraseToAnyPublisher(),
                favouriteFilter: self.favouritePublisher.eraseToAnyPublisher()
            )
        ).sink { state in

            guard case .success = state else { return }
            expectation.fulfill()

        }.store(in: &self.cancellables)

        self.searchPublisher.send("ship 1")

        wait(for: [expectation], timeout: .quickTimeout)

        XCTAssertEqual(viewModel.pirateShipsViewModel.count, 1)
    }
}

private class PirateShipsListingNavigatorMock: PirateShipsListingNavigator {

    var onDidTap: ((PirateShip) -> Void)!

    func didTap(with pirateShip: PirateShip) { self.onDidTap(pirateShip) }
}
