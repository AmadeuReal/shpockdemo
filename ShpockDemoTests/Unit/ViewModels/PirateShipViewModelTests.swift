//
//  PirateShipViewModelTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Combine
import XCTest
@testable import ShpockDemo

final class PirateShipViewModelTests: XCTestCase {

    var imageRequester: ImageRequesterMock!
    var cancellables = [AnyCancellable]()

    override func setUp() {
        super.setUp()

        self.imageRequester = ImageRequesterMock()

        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()
    }

    func testPriceWhenLocaleIsUS() {

        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = .init(identifier: "en_US")

        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }
        
        let viewModel = PirateShipViewModel(
            pirateShip: .mock(id: 1, price: 10),
            priceFormatter: formatter,
            imageRequester: self.imageRequester
        )
        XCTAssertEqual(viewModel.price, "$10.00")
    }

    func testPriceWhenLocaleIsGB() {

        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = .init(identifier: "en_GB")

        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }

        let viewModel = PirateShipViewModel(
            pirateShip: .mock(id: 1, price: 10),
            priceFormatter: formatter,
            imageRequester: self.imageRequester
        )
        XCTAssertEqual(viewModel.price, "£10.00")
    }

    func testPriceWhenLocaleIsAT() {

        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = .init(identifier: "de_AT")

        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }

        let viewModel = PirateShipViewModel(
            pirateShip: .mock(id: 1, price: 10),
            priceFormatter: formatter,
            imageRequester: self.imageRequester
        )

        XCTAssertEqual(viewModel.price, "€ 10,00")
    }

    func testPirateName() {

        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }
        
        let viewModel = PirateShipViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10),
            priceFormatter: .mock,
            imageRequester: self.imageRequester
        )

        XCTAssertEqual(viewModel.shipName, "The Black Pearl Pirate Ship")
    }

    func testFetchPirateImage() {

        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }

        let viewModel = PirateShipViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10),
            priceFormatter: .mock,
            imageRequester: self.imageRequester
        )

        let expectation = XCTestExpectation(description: "Fetch image should return expected values")

        viewModel.fetchImage()

        viewModel.$shipImage.sink { image in

            guard image == AssetsBook.mock else { return }
            expectation.fulfill()
            
        }.store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testFetchFailurePirateImage() {

        self.imageRequester.onFetchImageCallback = { .failure(NetworkingError.generic) }

        let viewModel = PirateShipViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10),
            priceFormatter: .mock,
            imageRequester: self.imageRequester
        )

        let expectation = XCTestExpectation(description: "Fetch image should return expected values")

        viewModel.$shipImage.sink { image in

            guard viewModel.shipImage == nil else { return }
            expectation.fulfill()
            
        }.store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }
}
