//
//  PirateShipDetailViewModelTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

import Combine
import XCTest
@testable import ShpockDemo

final class PirateShipDetailViewModelTests: XCTestCase {

    var imageRequester: ImageRequesterMock!
    var favouriteRequester: FavouriteShipsRequesterMock!

    var cancellables = [AnyCancellable]()
    var hearGreetingSubject: PassthroughSubject<Void, Never>!
    var favouriteSubject: PassthroughSubject<Void, Never>!

    override func setUp() {
        super.setUp()

        self.imageRequester = ImageRequesterMock()
        self.favouriteRequester = FavouriteShipsRequesterMock()

        self.hearGreetingSubject = PassthroughSubject<Void, Never>()
        self.favouriteSubject = PassthroughSubject<Void, Never>()

        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()
    }

    func testPriceWhenLocaleIsUS() {

        let formatter = NumberFormatter.mock
        formatter.locale = .init(identifier: "en_US")

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, price: 10),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )
        XCTAssertEqual(viewModel.shipPrice, "$10.00 (VAT included)")
    }

    func testPriceWhenLocaleIsGB() {

        let formatter = NumberFormatter.mock
        formatter.locale = .init(identifier: "en_GB")

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, price: 10),
            priceFormatter: formatter,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )
        XCTAssertEqual(viewModel.shipPrice, "£10.00 (VAT included)")
    }

    func testPriceWhenLocaleIsAT() {

        let formatter = NumberFormatter.mock
        formatter.locale = .init(identifier: "de_AT")

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, price: 10),
            priceFormatter: formatter,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        XCTAssertEqual(viewModel.shipPrice, "€ 10,00 (VAT included)")
    }

    func testPirateName() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        XCTAssertEqual(viewModel.shipName, "The Black Pearl Pirate Ship")
    }

    func testFetchPirateImage() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        self.imageRequester.onFetchImageCallback = { .success(AssetsBook.mock) }
        let expectation = XCTestExpectation(description: "Fetch image should return expected values")

        viewModel.shipImage.sink { completion in

            guard case .failure = completion else { return }

        } receiveValue: { image in

            guard image == AssetsBook.mock else { return }
            expectation.fulfill()

        }.store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testFetchFailurePirateImage() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10),
            priceFormatter: .mock,
            shipImage: .failure(NetworkingError.generic),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "Fetch image should return expected values")

        viewModel.shipImage.sink { completion in

            guard case .failure = completion else { return }
            expectation.fulfill()

        } receiveValue: { image in

            XCTFail("Image provided an expected image")

        }.store(in: &self.cancellables)

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testWhenCallingShowAlertWhenGreetingIsNil() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10, greeting: nil),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "it is expected to have the right title and description")

        let delegate = PirateShipDetailNavigatorMock()

        delegate.onDidTapToShowAlert = { title, description in

            guard title == "Hear the description mate", description == "Ahoi!" else { return }
            expectation.fulfill()
        }

        viewModel.navigator = delegate
        viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.favouriteSubject.eraseToAnyPublisher()
            )
        )

        self.hearGreetingSubject.send(())

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testWhenCallingShowAlertWhenGreetingIsAh() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10, greeting: .ah),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "it is expected to have the right title and description")

        let delegate = PirateShipDetailNavigatorMock()
        delegate.onDidTapToShowAlert = { title, description in

            guard title == "Hear the description mate", description == "Ahoi!" else { return }
            expectation.fulfill()
        }

        viewModel.navigator = delegate
        viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.favouriteSubject.eraseToAnyPublisher()
            )
        )

        self.hearGreetingSubject.send(())

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testWhenCallingShowAlertWhenGreetingIsAy() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10, greeting: .ay),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "it is expected to have the right title and description")

        let delegate = PirateShipDetailNavigatorMock()
        delegate.onDidTapToShowAlert = { title, description in

            guard title == "Hear the description mate", description == "Aye Aye!" else { return }
            expectation.fulfill()
        }

        viewModel.navigator = delegate
        viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.favouriteSubject.eraseToAnyPublisher()
            )
        )

        self.hearGreetingSubject.send(())

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testWhenCallingShowAlertWhenGreetingIsAr() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10, greeting: .ar),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "it is expected to have the right title and description")

        let delegate = PirateShipDetailNavigatorMock()
        delegate.onDidTapToShowAlert = { title, description in

            guard title == "Hear the description mate", description == "Arrr!" else { return }
            expectation.fulfill()
        }

        viewModel.navigator = delegate

        viewModel.navigator = delegate
        viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.favouriteSubject.eraseToAnyPublisher()
            )
        )

        self.hearGreetingSubject.send(())

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testWhenCallingShowAlertWhenGreetingIsYo() {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10, greeting: .yo),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "it is expected to have the right title and description")

        let delegate = PirateShipDetailNavigatorMock()
        delegate.onDidTapToShowAlert = { title, description in

            guard title == "Hear the description mate", description == "Yo ho hooo!" else { return }
            expectation.fulfill()
        }

        viewModel.navigator = delegate
        viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.favouriteSubject.eraseToAnyPublisher()
            )
        )

        self.hearGreetingSubject.send(())

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testWhenFavouritingAPirate() {

        self.favouriteRequester.onIsShipFavouritedCallback = { .success(false) }
        self.favouriteRequester.onSaveFavouriteShipCallback = { .success(()) }
        
        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10, greeting: .yo),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "it is expected to have the right title and description")

        let delegate = PirateShipDetailNavigatorMock()
        delegate.onDidTapToShowAlert = { title, _ in

            guard title == "Added to favourite" else { return }
            expectation.fulfill()
        }

        viewModel.navigator = delegate
        viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.favouriteSubject.eraseToAnyPublisher()
            )
        )

        self.favouriteSubject.send(())

        wait(for: [expectation], timeout: .quickTimeout)
    }

    func testWhenRemovePirateFromList() {

        self.favouriteRequester.onIsShipFavouritedCallback = { .success(true) }
        self.favouriteRequester.onRemoveFromFavouriteShipCallback = { .success(()) }

        let viewModel = PirateShipDetailViewModel(
            pirateShip: .mock(id: 1, title: "The Black Pearl Pirate Ship", price: 10, greeting: .yo),
            priceFormatter: .mock,
            shipImage: .success(AssetsBook.mock),
            favouriteRequester: self.favouriteRequester
        )

        let expectation = XCTestExpectation(description: "it is expected to have the right title and description")

        let delegate = PirateShipDetailNavigatorMock()
        delegate.onDidTapToShowAlert = { title, _ in

            guard title == "Removed from favourites" else { return }
            expectation.fulfill()
        }

        viewModel.navigator = delegate
        viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.favouriteSubject.eraseToAnyPublisher()
            )
        )

        self.favouriteSubject.send(())

        wait(for: [expectation], timeout: .quickTimeout)
    }
}
