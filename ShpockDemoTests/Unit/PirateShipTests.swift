//
//  PirateShipTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import XCTest
@testable import ShpockDemo

final class PirateShipTests: XCTestCase {

    private lazy var bundle = Bundle(for: Self.self)

    func testBuildingFromJSON() throws {

        let fileURL = try XCTUnwrap(self.bundle.url(forResource: "sample_ship_response", withExtension: "json"))
        let data = try Data(contentsOf: fileURL)
        let ships = try JSONDecoder().decode([PirateShip].self, from: data)

        XCTAssertEqual(ships.count, 10)
    }
}
