//
//  LocalStorageTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 14/11/2020.
//

import XCTest
@testable import ShpockDemo

final class LocalStorageTests: XCTestCase {

    var userDefaults: UserDefaults!

    private let decoder = JSONDecoder()
    private let encoder = JSONEncoder()

    override func setUp() {

        super.setUp()
        self.userDefaults = UserDefaults(suiteName: String(describing: Self.self))
    }

    override func tearDown() {

        self.userDefaults.removeSuite(named: String(describing: Self.self))
        super.tearDown()
    }

    func testStoreAPrimitiveValue() throws {

        let storage = LocalStorage(userDefaults: self.userDefaults, decoder: self.decoder, encoder: self.encoder)
        storage.save(3, in: "key")

        let entries = try storage.value(at: "key") as Int

        XCTAssertEqual(entries, 3)
    }

    func testStoreAnArrayOfStrings() throws {

        let storage = LocalStorage(userDefaults: self.userDefaults, decoder: self.decoder, encoder: self.encoder)
        storage.save(["value_1"], in: "key")

        let entries = try storage.value(at: "key") as [String]

        XCTAssertEqual(entries, ["value_1"])
    }

    func testWhenOverridingAnExistingValue() throws {

        let storage = LocalStorage(userDefaults: self.userDefaults, decoder: self.decoder, encoder: self.encoder)
        storage.save(["value_1"], in: "key")
        storage.save(["value_1", "value_2"], in: "key")

        let entries = try storage.value(at: "key") as [String]

        XCTAssertEqual(entries, ["value_1", "value_2"])
    }

    func testItCanSaveAndRetrieveACodableModel() throws {

        let storage = LocalStorage(userDefaults: self.userDefaults, decoder: self.decoder, encoder: self.encoder)
        storage.save(DummyCodableObject(name: "name"), in: "dummy")

        let value = try storage.value(at: "dummy") as DummyCodableObject

        XCTAssertEqual(value, DummyCodableObject(name: "name"))
    }

    func testRetrieveNonExistentValue() {

        let storage = LocalStorage(userDefaults: self.userDefaults, decoder: self.decoder, encoder: self.encoder)

        XCTAssertThrowsError(
            try storage.value(at: "key_1") as [String],
            "when fetching a non existing value from LocalStorage it is expected that a typed error is returned"
        ) {

            XCTAssertEqual($0 as? StorageError, .notFound)
        }
    }

    func testThrowAnErrorWhenRequestingADifferentTypeFromTheStoredValue() {

        let storage = LocalStorage(userDefaults: self.userDefaults, decoder: self.decoder, encoder: self.encoder)
        storage.save(["value_1"], in: "key")

        XCTAssertThrowsError(
            try storage.value(at: "key") as DummyCodableObject,
            "when the stored value is from a different data model we expect to receive a typed error"
        ) {

            XCTAssertEqual($0 as? StorageError, .typeMismatch)
        }
    }

    func testRemoveEntryFromKey() {

        let storage = LocalStorage(userDefaults: self.userDefaults, decoder: self.decoder, encoder: self.encoder)

        storage.save("value_1", in: "key")
        storage.deleteValue(fromKey: "key")

        XCTAssertThrowsError(
            try storage.value(at: "key") as String,
            "when fetching a non existing value from LocalStorage it is expected that a typed error is returned"
        ) {

            XCTAssertEqual($0 as? StorageError, .notFound)
        }
    }
}

private struct DummyCodableObject: Codable, Equatable {

    let name: String
}
