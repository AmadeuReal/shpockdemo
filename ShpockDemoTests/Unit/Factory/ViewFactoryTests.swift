//
//  ViewFactoryTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import XCTest
@testable import ShpockDemo

final class ViewFactoryTests: XCTestCase {

    var network: NetworkMock!
    var factory: ViewControllerFactory!

    override func setUp() {

        super.setUp()

        self.network = NetworkMock()
        self.factory = Factory(network: self.network)
    }

    func testBuildingPirateListingViewController() {

        let delegate = PirateShipsListingNavigatorMock()
        let viewController = self.factory.buildPirateShipsListViewController(navigator: delegate)

        XCTAssertTrue(viewController is PirateShipsListingViewController)
    }

    func testBuildingPirateDetailViewController() {

        let delegate = PirateShipDetailNavigatorMock()
        let viewController = self.factory.buildPirateShipDetailViewController(
            with: .mock(id: 1, price: 20),
            navigator: delegate
        )

        XCTAssertTrue(viewController is PirateShipDetailViewController)
    }
}

private final class PirateShipsListingNavigatorMock: PirateShipsListingNavigator {

    var onDidTapCallback: ((PirateShip) -> Void)!

    func didTap(with pirateShip: PirateShip) { self.onDidTapCallback(pirateShip) }
}
