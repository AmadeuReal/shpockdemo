//
//  CoordinatorFactoryTest.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

import XCTest
@testable import ShpockDemo

final class CoordinatorFactoryTest: XCTestCase {

    var network: NetworkMock!
    var factory: CoordinatorFactory!

    override func setUp() {

        super.setUp()

        self.network = NetworkMock()
        self.factory = Factory(network: self.network)
    }

    func testBuildingTheRootCoordinator() {

        let rootCoordinator = self.factory.buildRootCoordinator()
        XCTAssertNotNil(rootCoordinator)
    }
}
