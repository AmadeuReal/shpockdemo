//
//  RootCoordinatorTests.swift
//  ShpockDemoTests
//
//  Created by Amadeu de Martos on 13/11/2020.
//

import XCTest
@testable import ShpockDemo

final class RootCoordinatorTests: XCTestCase {

    var window: UIWindow!
    var coordinatorFactory: CoordinatorFactoryMock!
    var viewControllerFactory: ViewControllerFactoryMock!

    override func setUp() {

        super.setUp()

        self.window = UIWindow()
        self.coordinatorFactory = CoordinatorFactoryMock()
        self.viewControllerFactory = ViewControllerFactoryMock()
    }

    func testCoordinatorStart() {

        let coordinator = RootCoordinator(
            coordinatorFactory: self.coordinatorFactory,
            viewControllerFactory: self.viewControllerFactory,
            window: self.window
        )

        self.viewControllerFactory.onBuildPirateShipsListViewControllerCallback = { UIViewController() }

        coordinator.start()

        XCTAssertNotNil(self.window.rootViewController)
    }

    func testNavigationWhenItemIsTapped() {

        let coordinator = RootCoordinator(
            coordinatorFactory: self.coordinatorFactory,
            viewControllerFactory: self.viewControllerFactory,
            window: self.window
        )

        self.viewControllerFactory.onBuildPirateShipsListViewControllerCallback = { UIViewController() }
        coordinator.start()

        self.viewControllerFactory.onBuildPirateShipDetailViewControllerCallback = { ship in

            XCTAssertEqual(ship.id, 1)
            return UIViewController()
        }

        coordinator.didTap(with: .mock(id: 1, price: 10))

        XCTAssertEqual((self.window.rootViewController as? UINavigationController)?.viewControllers.count, 1)
    }

    func testNavigationWhenBuildingAlert() {

        let coordinator = RootCoordinator(
            coordinatorFactory: self.coordinatorFactory,
            viewControllerFactory: self.viewControllerFactory,
            window: self.window
        )

        let mockViewController = UIViewController()
        self.viewControllerFactory.onBuildPirateShipsListViewControllerCallback = { mockViewController }
        coordinator.start()

        coordinator.showAlertController(with: "Title", description: "Description")

        XCTAssertTrue(mockViewController.presentedViewController is UIAlertController)
    }
}
