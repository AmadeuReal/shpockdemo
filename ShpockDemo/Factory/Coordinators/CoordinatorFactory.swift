//
//  CoordinatorFactory.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Foundation

protocol CoordinatorFactory {

    func buildRootCoordinator() -> Coordinator
}
