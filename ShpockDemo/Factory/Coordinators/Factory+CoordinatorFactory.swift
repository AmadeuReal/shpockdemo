//
//  Factory+CoordinatorFactory.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

extension Factory: CoordinatorFactory {

    func buildRootCoordinator() -> Coordinator {

        RootCoordinator(coordinatorFactory: self, viewControllerFactory: self, window: UIWindow())
    }
}
