//
//  Factory+ViewControllerFactory.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

extension Factory: ViewControllerFactory {

    func buildPirateShipsListViewController(
        navigator: PirateShipsListingNavigator
    ) -> UIViewController {

        let viewModel = PirateShipsListingViewModel(
            priceFormatter: self.numberFormatter,
            shipsRequester: PirateShipsRequester(
                network: self.network,
                decoder: self.jsonDecoder,
                queue: .main
            ),
            imageRequester: self.imageRequester,
            favouriteShipsRequester: self.favouriteShipsRequester
        )
        viewModel.navigator = navigator

        let viewController = PirateShipsListingViewController(viewModel: viewModel)
        viewController.title = TextConstant.PirateShipsList.title

        return viewController
    }

    func buildPirateShipDetailViewController(
        with pirateShip: PirateShip,
        navigator: PirateShipDetailNavigator
    ) -> UIViewController {

        let viewModel = PirateShipDetailViewModel(
            pirateShip: pirateShip,
            priceFormatter: self.numberFormatter,
            shipImage: self.imageRequester.fetchImage(from: pirateShip.imageUrl),
            favouriteRequester: self.favouriteShipsRequester
        )
        viewModel.navigator = navigator

        let viewController = PirateShipDetailViewController(viewModel: viewModel)
        viewController.title = TextConstant.PirateShipDetail.title(id: pirateShip.id.description)

        return viewController
    }
}
