//
//  ViewControllerFactory.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

protocol ViewControllerFactory {

    func buildPirateShipsListViewController(
        navigator: PirateShipsListingNavigator
    ) -> UIViewController

    func buildPirateShipDetailViewController(
        with pirateShip: PirateShip,
        navigator: PirateShipDetailNavigator
    ) -> UIViewController
}
