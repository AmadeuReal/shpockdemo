//
//  Factory.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Foundation

final class Factory {

    let numberFormatter: NumberFormatter
    let network: NetworkType
    let imageRequester: ImageRequester
    let favouriteShipsRequester: FavouriteShipsRequester

    let jsonDecoder = JSONDecoder()
    let encoder = JSONEncoder()

    init(network: NetworkType = Network(session: .shared)) {

        self.network = network

        self.imageRequester = ImageRequester(
            network: self.network,
            queue: DispatchQueue.global(qos: .utility)
        )

        self.favouriteShipsRequester = FavouriteShipsRequester(
            storage: LocalStorage(
                userDefaults: .standard,
                decoder: self.jsonDecoder,
                encoder: self.encoder
            )
        )

        self.numberFormatter = NumberFormatter()
        // This is like this because the default value for Locale.current from a Simulator is `Locale(identifier: "en")`
        // and I would like to see the right currency :D
        self.numberFormatter.locale = .init(identifier: "de_AT")
        self.numberFormatter.numberStyle = .currency
    }
}
