//
//  Storage.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 14/11/2020.
//

import Foundation

protocol Storage {

    func save<T: Codable>(_ value: T, in key: String)
    func value<T: Codable>(at key: String) throws -> T
    func deleteValue(fromKey key: String)
}

enum StorageError: Error {

    case typeMismatch
    case notFound
}

final class LocalStorage: Storage {

    private let userDefaults: UserDefaults
    private let decoder: JSONDecoder
    private let encoder: JSONEncoder

    init(userDefaults: UserDefaults, decoder: JSONDecoder, encoder: JSONEncoder) {

        self.userDefaults = userDefaults
        self.decoder = decoder
        self.encoder = encoder
    }

    func save<T: Codable>(_ value: T, in key: String) {

        do {

            let data = try self.encoder.encode(value)
            self.userDefaults.set(data, forKey: key)

        } catch {

            if case .invalidValue? = error as? EncodingError {

                self.userDefaults.set(value, forKey: key)
            }
        }
    }

    func value<T: Codable>(at key: String) throws -> T {

        guard let value = self.userDefaults.value(forKey: key) else { throw StorageError.notFound }

        guard let data = value as? Data else { throw StorageError.typeMismatch }

        return try self.decode(data: data)
    }

    private func decode<T: Codable>(data: Data) throws -> T {

        do {

            return try self.decoder.decode(T.self, from: data)

        } catch {

            throw StorageError.typeMismatch
        }
    }

    func deleteValue(fromKey key: String) {

        self.userDefaults.removeObject(forKey: key)
    }
}
