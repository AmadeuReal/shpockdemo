//
//  Network.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Foundation
import Combine

protocol NetworkType {

    func request(
        with request: URLRequest,
        queue: DispatchQueue
    ) -> AnyPublisher<Data, Error>
}

final class Network: NetworkType {

    private static let successResponseRange = 200 ... 299

    private let session: URLSession

    public init(session: URLSession = URLSession(configuration: URLSessionConfiguration.default)) {

        self.session = session
    }

    func request(
        with request: URLRequest,
        queue: DispatchQueue
    ) -> AnyPublisher<Data, Error> {

        let path = request.url?.absoluteString ?? ""
        print("🔄 - Requesting \(path)")

        return self.session.dataTaskPublisher(for: request).tryMap {

            guard let response = $0.response as? HTTPURLResponse,
                  Self.successResponseRange ~= response.statusCode else {
                
                throw URLError(.badServerResponse)
            }

            print("✅ - Successful requested from \(path) [\(response.statusCode)]")

            return $0.data

        }.receive(on: queue)
        .eraseToAnyPublisher()
    }
}
