//
//  TextConstant.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Foundation

enum TextConstant {

    enum PirateShipsList {

        static let title = "Pirate Ships"
    }

    enum PirateShipDetail {

        static let greetingCTA = "Hear the greeting"
        static let noPrice = "-"

        static func title(id: String) -> String { String(format: "Ship #%@", id) }
        static func price(_ value: String) -> String { String(format: "%@ (VAT included)", value) }

        enum Greeting {

            static let alertTitle = "Hear the description mate"
            static let alertButton = "Cool cool cool!"

            static let ah = "Ahoi!"
            static let ay = "Aye Aye!"
            static let ar = "Arrr!"
            static let yo = "Yo ho hooo!"
        }

        enum Favourites {

            enum Add {

                static let title = "Added to favourite"
                static let description = "This ships now belongs to your favourite list :)"
            }

            enum Remove {

                static let title = "Removed from favourites"
                static let description = "This ships no longer now belongs to your favourite list"
            }
        }
    }
}
