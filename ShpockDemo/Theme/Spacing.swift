//
//  Spacing.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

struct Spacing {

    /// 4
    static let XS: CGFloat = 4
    /// 8
    static let S: CGFloat = 8
    /// 16
    static let M: CGFloat = 16
    /// 32
    static let L: CGFloat = 32
}
