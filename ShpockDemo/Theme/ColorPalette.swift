//
//  ColorPalette.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

enum ColorPalette {

    static let background = UIColor(hex: "#f5f0e5")

    /// #3f403f
    static let text = UIColor(hex: "#3f403f")

    /// #475841
    static let text2 = UIColor(hex: "#475841")

    /// #0075EB
    static let text3 = UIColor(hex: "#0075EB")
}
