//
//  A11y.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Foundation

public enum A11y {

    public enum PirateDetail {

        public static let image = "HomeView.image"
        public static let title = "HomeView.title"
        public static let detail = "HomeView.detail"
        public static let price = "HomeView.price"
        public static let greetingCTA = "HomeView.greetingCTA"
    }
}
