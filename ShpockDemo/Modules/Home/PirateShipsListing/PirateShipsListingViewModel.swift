//
//  PirateShipsListingViewModel.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Combine
import Foundation

protocol PirateShipsListingNavigator: AnyObject {

    func didTap(with pirateShip: PirateShip)
}

protocol PirateShipsListingViewModelType {

    var pirateShipsViewModel: [PirateShipViewModel] { get }
    func bind(input: PirateShipListingViewModelInput) -> PirateShipListingViewModelOuput
}

typealias PirateShipListingViewModelOuput = AnyPublisher<PirateShipListingState, Never>

enum PirateShipListingState {

    case loading
    case success
    case failure(Error)
}

struct PirateShipListingViewModelInput {

    let selection: AnyPublisher<Int, Never>
    let search: AnyPublisher<String, Never>
    let favouriteFilter: AnyPublisher<Void, Never>
}

final class PirateShipsListingViewModel {

    weak var navigator: PirateShipsListingNavigator?

    private let imageRequester: ImageRequesterType
    private let priceFormatter: NumberFormatter
    private let shipsRequester: PirateShipsRequesterType
    private let favouriteShipsRequester: FavouriteShipsRequesterType

    private var isFavouriteFilterActive = false
    private var searchTerm = ""

    private var filteredShips = [PirateShip]()
    private var pirateShips = [PirateShip]()
    var pirateShipsViewModel = [PirateShipViewModel]()
    private var cancellables: [AnyCancellable] = []

    init(
        priceFormatter: NumberFormatter,
        shipsRequester: PirateShipsRequesterType,
        imageRequester: ImageRequesterType,
        favouriteShipsRequester: FavouriteShipsRequesterType
    ) {

        self.priceFormatter = priceFormatter
        self.shipsRequester = shipsRequester
        self.imageRequester = imageRequester
        self.favouriteShipsRequester = favouriteShipsRequester
    }
}

// MARK: - PirateShipsListingViewModelType

extension PirateShipsListingViewModel: PirateShipsListingViewModelType {

    private func pirateShipViewModel(for pirateShip: PirateShip) -> PirateShipViewModel {

        .init(
            pirateShip: pirateShip,
            priceFormatter: self.priceFormatter,
            imageRequester: self.imageRequester
        )
    }

    func bind(input: PirateShipListingViewModelInput) -> PirateShipListingViewModelOuput {

        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()

        input.selection
            .sink(receiveValue: { [unowned self] row in

                if self.filteredShips.isEmpty == false {

                    self.navigator?.didTap(with: self.filteredShips[row])

                } else {

                    self.navigator?.didTap(with: self.pirateShips[row])
                }
            })
            .store(in: &self.cancellables)

        let favouriteFilter: PirateShipListingViewModelOuput = input.favouriteFilter
            .map { _ -> AnyPublisher<[Int], Never> in

                self.isFavouriteFilterActive.toggle()

                return self.favouriteShipsRequester.fetchFavouritedShipIds().replaceError(with: []).eraseToAnyPublisher()

        }.switchToLatest().map { ids -> PirateShipListingState in

            if self.isFavouriteFilterActive {

                self.filteredShips = self.pirateShips.filter { ids.contains($0.id) == self.isFavouriteFilterActive }

            } else {

                self.filteredShips = self.pirateShips
            }
            
            self.pirateShipsViewModel = self.filteredShips.map(self.pirateShipViewModel(for:))

            return .success

        }.eraseToAnyPublisher()

        let searchInput = input.search
            .debounce(for: .milliseconds(300), scheduler: RunLoop.main)
            .removeDuplicates()

        let search: PirateShipListingViewModelOuput = searchInput.map { term -> AnyPublisher<[PirateShip], Never> in

            self.searchTerm = term
            self.isFavouriteFilterActive = false

            return self.shipsRequester.fetchShips().map { ships -> [PirateShip] in

                guard term.isEmpty == false else { return ships }
                return ships.filter { $0.title?.lowercased().contains(term.lowercased()) == true }

            }.replaceError(with: []).eraseToAnyPublisher()

        }.switchToLatest().map { [unowned self] ships -> PirateShipListingState in

            self.filteredShips = ships
            self.pirateShips = ships
            self.pirateShipsViewModel = ships.map(self.pirateShipViewModel(for:))
            return .success

        }.eraseToAnyPublisher()

        let initialState: PirateShipListingViewModelOuput = Just(.loading).eraseToAnyPublisher()
        return Publishers.Merge3(initialState, search, favouriteFilter).eraseToAnyPublisher()
    }
}
