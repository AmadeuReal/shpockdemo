//
//  PirateShipsListingViewController.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Combine
import UIKit

final class PirateShipsListingViewController: UICollectionViewController {

    private var cancellables: [AnyCancellable] = []

    private let selection = PassthroughSubject<Int, Never>()
    private let search = PassthroughSubject<String, Never>()
    private let tapFavouriteSubject = PassthroughSubject<Void, Never>()

    private let activityIndicator: UIActivityIndicatorView = create()
    private let viewModel: PirateShipsListingViewModelType

    private static func createCollectionViewLayout() -> UICollectionViewLayout {

        let item = NSCollectionLayoutItem(
            layoutSize: .init(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1)
            )
        )
        item.contentInsets = .init(top: 0, leading: 0, bottom: Spacing.M, trailing: Spacing.M)

        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: .init(
                widthDimension: .fractionalWidth(1),
                heightDimension: .estimated(230)
            ),
            subitem: item,
            count: 2
        )

        let section = NSCollectionLayoutSection(group: group)

        section.contentInsets = .init(top: 0, leading: Spacing.M, bottom: 0, trailing: 0)

        return UICollectionViewCompositionalLayout(section: section)
    }

    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = true
        searchController.searchBar.tintColor = UIColor.black
        searchController.searchBar.delegate = self
        return searchController
    }()

    lazy var dataSource = DataSource(
        collectionView: self.collectionView,
        cellProvider: { collectionView, indexPath, viewModel in

            let cell = collectionView.dequeue(PirateShipView.self, for: indexPath)
            cell.configure(with: viewModel)
            return cell
        }
    )

    init(viewModel: PirateShipsListingViewModel) {

        self.viewModel = viewModel
        super.init(collectionViewLayout: Self.createCollectionViewLayout())
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.searchController = self.searchController
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .bookmarks, target: self, action: #selector(self.didTapFavoriteButton))

        self.collectionView.backgroundView = self.activityIndicator
        self.collectionView.backgroundColor = ColorPalette.background
        self.collectionView.register(PirateShipView.self)

        NSLayoutConstraint.activate([

            self.activityIndicator.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.activityIndicator.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.activityIndicator.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.activityIndicator.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])

        self.applySnapshot(animatingDifferences: false)

        self.bindViewToModel()
        self.search.send("")
    }

    private func bindViewToModel() {

        self.viewModel.bind(
            input: .init(
                selection: self.selection.eraseToAnyPublisher(),
                search: self.search.eraseToAnyPublisher(),
                favouriteFilter: self.tapFavouriteSubject.eraseToAnyPublisher()
            )
        ).sink { self.render(state: $0) }
        .store(in: &self.cancellables)
    }

    func render(state: PirateShipListingState) {

        switch state {

        case .success:

            self.activityIndicator.stopAnimating()
            self.applySnapshot(animatingDifferences: true)

        case .loading:
            self.activityIndicator.startAnimating()
            
        case .failure:
            break
        }
    }

    @objc
    private func didTapFavoriteButton() {

        self.tapFavouriteSubject.send(())
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        self.selection.send(indexPath.row)
    }
}

//MARK: - UISearchBarDelegate

extension PirateShipsListingViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        self.search.send(searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        self.search.send("")
    }
}

extension PirateShipsListingViewController {
    enum Section {
        case main
    }

    typealias DataSource = UICollectionViewDiffableDataSource<Section, PirateShipViewModel>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, PirateShipViewModel>

    func applySnapshot(animatingDifferences: Bool) {

        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(self.viewModel.pirateShipsViewModel)
        self.dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
}
