//
//  PirateShipViewModel.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Combine
import UIKit

final class PirateShipViewModel {

    private var cancellable: AnyCancellable?

    private let imageRequester: ImageRequesterType
    private let priceFormatter: NumberFormatter
    private let pirateShip: PirateShip

    var shipId: Int { self.pirateShip.id }
    var price: String { self.priceFormatter.string(from: self.pirateShip.price as NSNumber) ?? TextConstant.PirateShipDetail.noPrice }
    var shipName: String? { self.pirateShip.title }

    @Published
    var shipImage: UIImage?

    init(pirateShip: PirateShip, priceFormatter: NumberFormatter, imageRequester: ImageRequesterType) {

        self.pirateShip = pirateShip
        self.priceFormatter = priceFormatter
        self.imageRequester = imageRequester

        self.fetchImage()
    }

    deinit {

        self.cancellable?.cancel()
    }

    func fetchImage() {

        self.cancellable = self.imageRequester.fetchImage(from: self.pirateShip.imageUrl)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in

                guard case .failure = completion else { return }
                self.shipImage = nil

            }, receiveValue: {

                self.shipImage = $0
            }
        )
    }
}

extension PirateShipViewModel: Hashable {

    static func == (lhs: PirateShipViewModel, rhs: PirateShipViewModel) -> Bool {

        lhs.shipId == rhs.shipId
    }

    func hash(into hasher: inout Hasher) {

        hasher.combine(self.shipId)
    }
}
