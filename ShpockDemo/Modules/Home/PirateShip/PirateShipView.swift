//
//  PirateShipView.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Combine
import UIKit

final class PirateShipView: UICollectionViewCell {

    private var cancellable: AnyCancellable?

    private let stackView: UIStackView = create {

        $0.distribution = .equalSpacing
        $0.axis = .vertical
        $0.spacing = Spacing.XS
    }

    private let imageView: UIImageView = create {

        $0.backgroundColor = ColorPalette.text
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
        $0.layer.cornerRadius = Spacing.M
    }

    private let titleLabel: UILabel = create {

        $0.textColor = ColorPalette.text
        $0.numberOfLines = 2
        $0.font = .systemFont(ofSize: 14, weight: .light)
    }

    private let priceLabel: UILabel = create {

        $0.textColor = ColorPalette.text2
        $0.font = .systemFont(ofSize: 14, weight: .bold)
    }

    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override init(frame: CGRect) {

        super.init(frame: frame)

        self.defineSubviews()
        self.defineSubviewsConstraints()

        self.layer.cornerRadius = 4
    }

    override func prepareForReuse() {

        super.prepareForReuse()

        self.cancellable?.cancel()
        self.titleLabel.text = nil
        self.priceLabel.text = nil
        self.imageView.image = nil
    }

    func configure(with viewModel: PirateShipViewModel) {

        self.titleLabel.text = viewModel.shipName
        self.priceLabel.text = viewModel.price
        self.cancellable = viewModel.$shipImage.sink(receiveValue: { [unowned self] image in
            
            UIView.transition(
                with: self.imageView,
                duration: 0.3,
                options: [.curveEaseOut, .transitionCrossDissolve],
                animations: {
                    self.imageView.image = image
                }
            )
        })
    }
}

private extension PirateShipView {

    func defineSubviews() {

        self.contentView.addSubview(self.imageView)

        self.stackView.addArrangedSubview(self.titleLabel)
        self.stackView.addArrangedSubview(self.priceLabel)

        self.contentView.addSubview(self.stackView)
    }

    func defineSubviewsConstraints() {

        NSLayoutConstraint.activate([

            self.imageView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: 0.7),

            self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            self.imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            self.imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),

            self.stackView.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: Spacing.S),
            self.stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            self.stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            self.stackView.bottomAnchor.constraint(lessThanOrEqualTo: self.contentView.bottomAnchor)
        ])
    }
}
