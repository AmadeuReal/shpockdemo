//
//  PirateShipDetailViewController.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Combine
import UIKit

final class PirateShipDetailViewController: UIViewController {

    private var imageCancelable: AnyCancellable?
    private let hearGreetingSubject = PassthroughSubject<Void, Never>()
    private let tapFavouriteSubject = PassthroughSubject<Void, Never>()

    private let stackView: UIStackView = create {

        $0.axis = .vertical
        $0.spacing = Spacing.M
    }

    private let imageView: UIImageView = create {

        $0.contentMode = .scaleAspectFit
        $0.accessibilityIdentifier = A11y.PirateDetail.image
    }

    private let titleLabel: UILabel = create {

        $0.textColor = ColorPalette.text
        $0.numberOfLines = 2
        $0.font = .systemFont(ofSize: 20, weight: .bold)
        $0.accessibilityIdentifier = A11y.PirateDetail.title
    }

    private let detailLabel: UILabel = create {

        $0.textColor = ColorPalette.text
        $0.numberOfLines = 0
        $0.font = .systemFont(ofSize: 14, weight: .light)
        $0.accessibilityIdentifier = A11y.PirateDetail.detail
    }

    private let priceLabel: UILabel = create {

        $0.textColor = ColorPalette.text2
        $0.font = .systemFont(ofSize: 14, weight: .bold)
        $0.accessibilityIdentifier = A11y.PirateDetail.price
    }

    private let greetingsButton: UIButton = create {

        $0.backgroundColor = ColorPalette.text
        $0.setTitle(TextConstant.PirateShipDetail.greetingCTA, for: .normal)
        $0.titleLabel?.font = .systemFont(ofSize: 14, weight: .bold)
        $0.layer.cornerRadius = Spacing.XS
        $0.accessibilityIdentifier = A11y.PirateDetail.greetingCTA
    }

    private let viewModel: PirateShipDetailViewModelType

    init(viewModel: PirateShipDetailViewModelType) {

        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .bookmarks, target: self, action: #selector(self.didTapFavoriteButton))
        self.greetingsButton.addTarget(self, action: #selector(self.didTapGreetingButton), for: .touchUpInside)

        self.view.backgroundColor = ColorPalette.background

        self.defineSubviews()
        self.defineSubviewsConstraints()

        self.configure()
    }

    deinit { self.imageCancelable?.cancel() }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    @objc
    private func didTapGreetingButton() { self.hearGreetingSubject.send(()) }

    @objc
    private func didTapFavoriteButton() { self.tapFavouriteSubject.send(()) }
}

private extension PirateShipDetailViewController {

    func defineSubviews() {

        self.view.addSubview(self.imageView)
        self.view.addSubview(self.stackView)

        self.stackView.addArrangedSubview(self.titleLabel)
        self.stackView.addArrangedSubview(self.detailLabel)
        self.stackView.addArrangedSubview(self.priceLabel)
        self.stackView.addArrangedSubview(self.greetingsButton)
    }

    func defineSubviewsConstraints() {

        NSLayoutConstraint.activate([

            self.imageView.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            self.imageView.heightAnchor.constraint(equalTo: self.imageView.widthAnchor, multiplier: 9 / 16),

            self.greetingsButton.heightAnchor.constraint(equalToConstant: 44),

            self.imageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.imageView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            self.imageView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),

            self.stackView.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: Spacing.M),
            self.stackView.leadingAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.leadingAnchor,
                constant: Spacing.S
            ),
            self.stackView.trailingAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.trailingAnchor,
                constant: -Spacing.S
            ),
            self.stackView.bottomAnchor.constraint(lessThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

    func configure() {

        self.viewModel.bind(
            input: .init(
                hearGreeting: self.hearGreetingSubject.eraseToAnyPublisher(),
                tapFavouriteSubject: self.tapFavouriteSubject.eraseToAnyPublisher()
            )
        )

        self.imageCancelable = self.viewModel.shipImage.sink { [unowned self] completion in

            self.imageCancelable?.cancel()

            guard case .failure = completion else { return }
            self.imageView.image = nil

        } receiveValue: { [unowned self] image in

            self.imageView.image = image
        }

        self.titleLabel.text = self.viewModel.shipName
        self.detailLabel.text = self.viewModel.shipDescription
        self.priceLabel.text = self.viewModel.shipPrice
    }
}
