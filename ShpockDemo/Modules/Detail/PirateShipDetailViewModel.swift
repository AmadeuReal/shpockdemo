//
//  PirateShipDetailViewModel.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 12/11/2020.
//

import Combine
import UIKit

protocol PirateShipDetailNavigator: AnyObject {

    func showAlertController(with title: String, description: String)
}

protocol PirateShipDetailViewModelType {

    var shipName: String? { get }
    var shipPrice: String { get }
    var shipDescription: String { get }

    var shipImage: AnyPublisher<UIImage, Error> { get }

    func bind(input: PirateShipDetailViewModelInput)
}

struct PirateShipDetailViewModelInput {

    let hearGreeting: AnyPublisher<Void, Never>
    let tapFavouriteSubject: AnyPublisher<Void, Never>
}

final class PirateShipDetailViewModel {

    private var cancellables = [AnyCancellable]()
    weak var navigator: PirateShipDetailNavigator?

    private let favouriteRequester: FavouriteShipsRequesterType
    private let pirateShip: PirateShip
    private let priceFormatter: NumberFormatter

    let shipImage: AnyPublisher<UIImage, Error>

    var shipPrice: String {

        guard let priceString = self.priceFormatter.string(from: self.pirateShip.price as NSNumber) else {

            return TextConstant.PirateShipDetail.noPrice
        }

        return TextConstant.PirateShipDetail.price(priceString)
    }

    var shipName: String? { self.pirateShip.title }
    var shipDescription: String { self.pirateShip.description }

    init(
        pirateShip: PirateShip,
        priceFormatter: NumberFormatter,
        shipImage: AnyPublisher<UIImage, Error>,
        favouriteRequester: FavouriteShipsRequesterType
    ) {

        self.pirateShip = pirateShip
        self.priceFormatter = priceFormatter
        self.shipImage = shipImage
        self.favouriteRequester = favouriteRequester
    }
}

//MARK: - PirateShipDetailViewModelType {

extension PirateShipDetailViewModel: PirateShipDetailViewModelType {

    func bind(input: PirateShipDetailViewModelInput) {

        self.cancellables.forEach { $0.cancel() }
        self.cancellables.removeAll()

        input.hearGreeting
            .sink(receiveValue: { [unowned self] in

                self.showHearingAlert()

            })
            .store(in: &self.cancellables)

        input.tapFavouriteSubject.flatMap { [unowned self] in

            self.favouriteRequester.isShipFavourited(id: self.pirateShip.id).flatMap { isFavourited in

                isFavourited ?
                    self.favouriteRequester.removeFromFavouriteShip(id: self.pirateShip.id).map { true } :
                    self.favouriteRequester.saveFavouriteShip(id: self.pirateShip.id).map { false }

            }.replaceError(with: false)

        }.sink(
            receiveValue: { didRemove in

                if didRemove {

                    self.navigator?.showAlertController(with: TextConstant.PirateShipDetail.Favourites.Remove.title, description: TextConstant.PirateShipDetail.Favourites.Remove.description)

                } else {

                    self.navigator?.showAlertController(with: TextConstant.PirateShipDetail.Favourites.Add.title, description: TextConstant.PirateShipDetail.Favourites.Add.description)
                }
            }
            
        ).store(in: &self.cancellables)
    }

    private func showHearingAlert() {

        let description: String

        switch self.pirateShip.greeting {
        case .none, .ah:

            description = TextConstant.PirateShipDetail.Greeting.ah

        case .ay:
            description = TextConstant.PirateShipDetail.Greeting.ay

        case .ar:
            description = TextConstant.PirateShipDetail.Greeting.ar

        case .yo:

            description = TextConstant.PirateShipDetail.Greeting.yo
        }

        self.navigator?.showAlertController(
            with: TextConstant.PirateShipDetail.Greeting.alertTitle,
            description: description
        )
    }
}
