//
//  RootCoordinator.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

final class RootCoordinator: Coordinator {

    private let coordinatorFactory: CoordinatorFactory
    private let viewControllerFactory: ViewControllerFactory

    private var childCoordinators: [Coordinator] = []

    private let window: UIWindow
    private let rootViewController = UINavigationController()

    init(
        coordinatorFactory: CoordinatorFactory,
        viewControllerFactory: ViewControllerFactory,
        window: UIWindow
    ) {

        self.coordinatorFactory = coordinatorFactory
        self.viewControllerFactory = viewControllerFactory

        self.window = window
    }

    func start() {

        let home = self.viewControllerFactory.buildPirateShipsListViewController(navigator: self)

        self.rootViewController.setViewControllers([home], animated: false)

        self.window.rootViewController = self.rootViewController

        self.window.makeKeyAndVisible()
    }
}

// MARK: - HomeViewControllerNavigator

extension RootCoordinator: PirateShipsListingNavigator {

    func didTap(with pirateShip: PirateShip) {

        let viewController = self.viewControllerFactory.buildPirateShipDetailViewController(
            with: pirateShip,
            navigator: self
        )

        self.rootViewController.pushViewController(viewController, animated: true)
    }
}

// MARK: - PirateShipDetailNavigator

extension RootCoordinator: PirateShipDetailNavigator {

    func showAlertController(with title: String, description: String) {

        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alertController.addAction(
            .init(
                title: TextConstant.PirateShipDetail.Greeting.alertButton,
                style: .default,
                handler: nil
            )
        )

        self.rootViewController.present(
            alertController,
            animated: true,
            completion: nil
        )
    }
}
