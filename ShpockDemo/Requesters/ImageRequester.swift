//
//  ImageRequester.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit
import Combine

protocol ImageRequesterType {

    func fetchImage(from url: String) -> AnyPublisher<UIImage, Error>
}

enum NetworkingError: Error {

    case generic
    case badData
}

final class ImageRequester: ImageRequesterType {

    private let cache: NSCache<NSString, UIImage>
    private let network: NetworkType
    private let queue: DispatchQueue

    init(network: NetworkType, queue: DispatchQueue) {

        self.network = network
        self.queue = queue

        self.cache = .init()
    }

    func fetchImage(from url: String) -> AnyPublisher<UIImage, Error> {

        let cacheKey = NSString(string: url)

        if let cachedImage = self.cache.object(forKey: cacheKey) {

            return Future { $0(.success(cachedImage)) }.eraseToAnyPublisher()
        }

        guard let url = URL(string: url) else {

            return Fail(error: NetworkingError.generic).eraseToAnyPublisher()
        }

        return self.network.request(
            with: URLRequest(url: url),
            queue: self.queue
        ).receive(on: DispatchQueue.main)
        .tryMap { data in

            guard let image = UIImage(data: data) else {

                print("❌ - Failed to transform data to UIImage from \(url)")
                throw NetworkingError.badData
            }

            self.cache.setObject(image, forKey: cacheKey)

            return image

        }.eraseToAnyPublisher()
    }
}
