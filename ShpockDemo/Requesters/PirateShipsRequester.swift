//
//  PirateShipsRequester.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Combine
import Foundation

protocol PirateShipsRequesterType {

    func fetchShips() -> AnyPublisher<[PirateShip], Error>
}

final class PirateShipsRequester: PirateShipsRequesterType {

    private let network: NetworkType
    private let queue: DispatchQueue
    private let decoder: JSONDecoder

    init(network: NetworkType, decoder: JSONDecoder, queue: DispatchQueue) {

        self.network = network
        self.decoder = decoder
        self.queue = queue
    }

    func fetchShips() -> AnyPublisher<[PirateShip], Error> {

        guard let url = URL(
            string: "https://assets.shpock.com/mobile/interview-test/pirateships"
        ) else {

            return Fail(error: NetworkingError.generic).eraseToAnyPublisher()
        }

        return self.network.request(
            with: URLRequest(url: url),
            queue: self.queue
        ).tryMap { data in

            let response = try self.decoder.decode(PirateRequestResponse.self, from: data)
            return response.ships.compactMap { $0 }

        }.eraseToAnyPublisher()
    }
}

private struct PirateRequestResponse: Decodable {

    let success: Bool
    let ships: [PirateShip?]
}
