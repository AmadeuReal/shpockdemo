//
//  FavouriteShipsRequester.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 14/11/2020.
//

import Combine

protocol FavouriteShipsRequesterType {

    func isShipFavourited(id: Int) -> AnyPublisher<Bool, Error>
    func saveFavouriteShip(id: Int) -> AnyPublisher<Void, Error>
    func removeFromFavouriteShip(id: Int) -> AnyPublisher<Void, Error>
    func fetchFavouritedShipIds() -> AnyPublisher<[Int], Error>
}

final class FavouriteShipsRequester: FavouriteShipsRequesterType {

    private let storageKey = "customer_favourite_ships"
    private let storage: Storage

    init(storage: Storage) {

        self.storage = storage
    }

    func fetchFavouritedShipIds() -> AnyPublisher<[Int], Error> {

        Future { completion in

            do {

                let storedIds = try self.storage.value(at: self.storageKey) as [Int]
                completion(.success(storedIds))

            } catch StorageError.notFound {

                completion(.success([]))

            } catch {

                completion(.failure(error))
            }

        }.eraseToAnyPublisher()
    }

    func saveFavouriteShip(id: Int) -> AnyPublisher<Void, Error> {

        self.fetchFavouritedShipIds().map { ids in

            guard ids.contains(id) == false else { return }
            self.storage.save(ids + [id], in: self.storageKey)

        }.eraseToAnyPublisher()
    }

    func isShipFavourited(id: Int) -> AnyPublisher<Bool, Error> {

        self.fetchFavouritedShipIds().map { $0.contains(id) }.eraseToAnyPublisher()
    }

    func removeFromFavouriteShip(id: Int) -> AnyPublisher<Void, Error> {

        self.fetchFavouritedShipIds().map { ids in

            let newIds = ids.filter { $0 != id }
            self.storage.save(newIds, in: self.storageKey)

        }.eraseToAnyPublisher()
    }
}
