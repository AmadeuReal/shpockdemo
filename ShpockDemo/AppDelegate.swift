//
//  AppDelegate.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {

    private let factory = Factory()

    private lazy var rootCoordinator = self.factory.buildRootCoordinator()

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        UINavigationBar.appearance().prefersLargeTitles = true

        self.rootCoordinator.start()

        return true
    }
}
