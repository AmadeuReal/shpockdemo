//
//  PirateShip.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import Foundation

struct PirateShip: Decodable {

    let id: Int
    let title: String?
    let description: String
    let price: Double
    let imageUrl: String
    let greeting: Greeting?

    enum Greeting: String, Decodable {

        case ah
        case ay
        case ar
        case yo
    }

    enum CodingKeys: String, CodingKey {

        case id
        case title
        case description
        case price
        case imageUrl = "image"
        case greeting = "greeting_type"
    }
}
