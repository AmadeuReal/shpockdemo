//
//  Reusable.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

protocol Reusable {

    static var reuseIdentifier: String { get }
}

extension Reusable where Self: UICollectionViewCell {

    static var reuseIdentifier: String { String(describing: self) }
}

extension UICollectionViewCell: Reusable {}
