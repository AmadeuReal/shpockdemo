//
//  UIView.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

func create<T: UIView>(configurate: ((T) -> Void) = { _ in }) -> T {

    let view = T()
    view.translatesAutoresizingMaskIntoConstraints = false
    configurate(view)

    return view
}
