//
//  UICollectionView.swift
//  ShpockDemo
//
//  Created by Amadeu de Martos on 11/11/2020.
//

import UIKit

extension UICollectionView {

    func register<T: UICollectionViewCell>(_ cellClass: T.Type) {

        self.register(cellClass, forCellWithReuseIdentifier: cellClass.reuseIdentifier)
    }

    func dequeue<T: UICollectionViewCell>(_ classType: T.Type, for indexPath: IndexPath) -> T {

        self.dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
}
